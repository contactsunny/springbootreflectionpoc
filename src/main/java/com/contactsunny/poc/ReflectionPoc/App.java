package com.contactsunny.poc.ReflectionPoc;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.reflect.Method;

@SpringBootApplication
public class App implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }

    @Override
    public void run(String... strings) throws Exception {

        String class1Name = "com.contactsunny.poc.ReflectionPoc.Class1";
        String class2Name = "com.contactsunny.poc.ReflectionPoc.Class2";

        Class class1Class = Class.forName(class1Name);

        String class1Method1Name = "class1Method1";

        Method class1Method1 = class1Class.getMethod(class1Method1Name);

        /*
        Throws NullPointerException because the first parameter to the invoke() method is null.
        We have to send an instance of the class that we're trying to use Reflection on.
         */
        try {
            class1Method1.invoke(null, null);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        Class1 class1 = new Class1();

        /*
        This works because I'm passing an instance of the class.
         */
        try {
            class1Method1.invoke(class1, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        BaseClass baseClass = new BaseClass();

        /*
        Trying to pass an instance of the base class instead of the child class.
        This will thrown an IllegalArgumentException with error "object is not an instance of declaring class"
         */
        try {
            class1Method1.invoke(baseClass, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
